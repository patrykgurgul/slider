$(document).ready(function(){

	//takie sobie zmienne
	var slidesCount = $('.slide').size();
	var which = 0;
	var itemWidth = $('.slide').width();
	var itemHeight = $('.slide').height();
	var animationTime = 500;
	var sliderDelay = 2500;
	var direction = ["left","right","top","bottom"];
	var ready = true;
	var whichPrevious = slidesCount-1;

	// @review:
	// $('.slide') powinno być cachowane do zmiennej. W innym wypadku niepotrzebnie kilka razy przeszukuje cały DOM
	// var slide = $('.slide');
	// inne rzeczy powinny być znajdywane
	// slide.find('h1') etc.

	//konstruktor slide
	function Slide(x)
	{
		this.item = $('.slide').eq(x);
		this.header = $('.slide h1').eq(x);
		this.subheader = $('.slide h2').eq(x);
		this.paragraph = $('.slide p').eq(x);
		this.image = $('.slide .text img').eq(x);
		this.button = $('.slide button').eq(x);
	}

	//metody klasy Slide
	//chowanie mięsa
	Slide.prototype.hideContent = function()
	{
		this.header.hide();
		this.subheader.hide();
		this.paragraph.hide();
		this.image.hide();
		this.button.hide();
	}

	//pokazywanie mięsa
	Slide.prototype.showContent = function()
	{
		var that = this;
		this.header.show('slide');
		setTimeout(function(){
			that.subheader.show('slide');
			setTimeout(function(){
				that.paragraph.show('scale');
				that.button.show('explode');
				setTimeout(function(){
					that.image.show('scale');
				}, 400);
			}, 400);
		}, 100);
	}

	//przesuwanie slajdu
	Slide.prototype.nextSlide = function(x, direction)
	{
		
		//var whichPrevious = (x < 1) ? slidesCount-1 : x-1;
		var o = {};
		var oMinus = {};

		

		function makeObject(d) //tworzenie obiektu wg schematu: " direction: distance "
		{
			switch(d)	//dla left przydziela left: +=itemWidth dla right przydziela left: -=itemWidth
			{
				case "left":
					o["left"] = "+="+itemWidth;
					oMinus["left"] = "-="+itemWidth;
					break;
				case "top":
					o["top"] = "+="+itemHeight;
					oMinus["top"] = "-="+itemHeight;
					break;
				case "right":
					o["left"] = "-="+itemWidth;
					oMinus["left"] = "+="+itemWidth;
					break;
				case "bottom":
					o["top"] = "-="+itemHeight;
					oMinus["top"] = "+="+itemHeight;
					break;

				// brak defaultowego case
			}
		}

		makeObject(direction);
		
		slides[whichPrevious].item.animate(o, animationTime, function(){
			slides[whichPrevious].item.hide().animate(oMinus, 1);
			slides[whichPrevious].hideContent();
		});
			slides[x].item.animate(oMinus,1).show().animate(o, animationTime, function(){
			slides[x].showContent();
			setButton(x);
			whichPrevious = x;
		});

		which++;
		delete o, oMinus;
	} 




	//tworzenie slajdów jako obiekt
	var slides = new Array();
	// @review:
	// obiekty typu array tworzymy za pomocą literałów:  var slides = [];
	// prawie niczym sie nie różni a jest dużo szybsze


	for (var i = 0; i <= slidesCount; i++)
	{
		slides[i] = new Slide(i);
	}

	//chowanie wszystkiego
	for (var i = 0; i <= slidesCount; i++)
	{
		slides[i].hideContent();
		slides[i].item.hide();
	}

	//startowanie animacji
	
	animateSlider();
	// @review 
	// wywołanie funkcji przed jej zdefiniowaniem = błąd logiczny
	// przeczytaj co to "hoisting"

	function animateSlider()
	{
		
		if(which >= slidesCount) which = 0;
		// jak nie jesteś pro to odbij od notacji bez bracketów {}

		$('body').on('click', '.slider-navigation', function(){ ready = false; which = parseInt($(this).attr("rel"));});
		
		if(!ready)
		{
			slides[which].nextSlide(which, direction[Math.round(Math.random()*3)]);
			setTimeout(function(){ animateSlider(); ready = true; }, sliderDelay);
		} else {
			slides[which].nextSlide(which, direction[Math.round(Math.random()*3)]);
			setTimeout(function(){				
				animateSlider();
			}, sliderDelay);					
		}
	}



	//dodaje nawigacje do slidera
	for(var i=0;i<=slidesCount-1;i++)
	{
		$('.btn-group').append("<button rel=\""+i+"\" class=\"btn button-default slider-navigation\">"+(i+1)+"</button>");
	}
	// @review
	// operacje bezpośrednio na DOM są bardzo kosztowne, więc trzeba je zmniejszać do minimum.
	// Powinieneś stworzyć sobie ciąg znaków w stringu i dodać je potem do btn-group. Polecam konstrukcje:
	
	// ;(function(){
	// 	var html = '';

	// 	for(var i=0;i<=slidesCount-1;i++) {
	// 		html += "<button rel=\""+i+"\" class=\"btn button-default slider-navigation\">"+(i+1)+"</button>";
	// 	}

	// 	$('.btn-group').append(html);

	// })();


	//zaznaczanie aktywnego slide'a na nawigacji
	function setButton(w)
	{
		$('.btn-group button').removeClass('btn-primary');
		$('.btn-group button').eq(w).toggleClass('btn-primary', [50]);
	}
	
	// @review
	// tyczy sie wszystkich funkcji. Definiuj funkcje tak:
	// var setButton = function(w){

	// }

	// @review
	// staraj sie używać czterech spacji zamiast tabulatora (do skonfigurowania w edytorze)

});